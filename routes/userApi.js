const userControler = require('../controlers/userControler');
const User = require('../models/user');

const { route } = require('./api');

module.exports = (router) => {

    router.route('/register')
    .post(userControler.register)

    router.route('/favorites/:id')
    .put(userControler.update)

    // route.route('/user')
    // .get(userControler.getUser)

    // router.route('./user')   to be defined
    // .get()
}