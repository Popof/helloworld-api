const mealControler = require("../controlers/mealControler");


module.exports = (router) => {

    router.route('/meal')
        .get(mealControler.getAll)
        .post(mealControler.add)

    router.route('/meal/:id')
        .get(mealControler.getById)
        .delete(mealControler.delete)
}