const express = require('express');
const User = require('../models/user');


// Creation router
const router = express.Router();

// Import fichier js avec fonctions router
require('./mealApi')(router);
require('./userApi')(router);

//Update User Favorites
// router.put('/favorites/:id', function(req,res){
//     User.findByIdAndUpdate({_id : req.params.id}, req.body)
// })

// Export du router
module.exports=router

