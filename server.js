const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const routesApi = require('./routes/api');
const passport = require ('passport');
passportLolac = require('passport-local').Strategy;
const cookieParser = require('cookie-parser');
const bcrypt = require('bcrypt');
const session = require('express-session');


// Connection à la base mongoDB
const db = require('./database.js');


// Création de la web API
const app = express();


// Utilisation de cors 
app.use(cors(
   {
       origin : "http://localhost:3000",
       credentials : true
   }
));


// Utilisation du middleware pour travailler en JSON
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended : true}));

// Session
app.use(session( {
    secret: "secretcode",
    resave : true,
    saveUninitialized : true
}))

// Cookie Parser
app.use(cookieParser("secretcode"));

// Passport
app.use(passport.initialize());
app.use(passport.session());
require('./passportConfig')(passport);

// Session
app.post("/connect", (req, res, next) => {
    passport.authenticate("local", (err, user, info) => {
        if (err) throw err;
        if (!user) res.send("Invalid email");
        else {
            req.logIn(user, err => {
                if (err) throw err;
                res.send(req.user);
                //res.send('Successfully Authenticated');
                console.log(req.user);
            })
        }
    })(req, res, next);
})
app.get('/user', (req, res) => {
    res.send(req.user);
})

app.get("/logout",function(req, res){
    try{ 
    console.log(req.user)
    req.logOut();
    console.log("log out back");
    delete req.session;
    console.log("deleted")}
    catch{
        console.log("logout error")
    }
  });

// Utilisation des routes
app.use('/api', routesApi)


// Démarrage du serveur
app.listen(
    1617, () => {
        console.log("Serveur api sur port 1617")
    }
);



