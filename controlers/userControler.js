const User = require('../models/user');
const bcrypt = require('bcrypt');


module.exports = {

    // Methode login
    addUser : (req, res) => {
        const user = User(req.body);

        user.save()
        .then(user => {
            res.status(200).json(user);
        })
        .catch(error => {
            res.status(500).json(error);
        })

        res.status(200).json({message : "Ajouté à la base de données"})
    },
    
    register : (req, res) => {
        User.findOne({ email : req.body.email}, async (err, doc) => {
            if (err) throw err;
            if (doc) res.send("User already exists");
            if (!doc) {
                const hashedPassword = await bcrypt.hash(req.body.password, 10);
                const newUser = new User({
                    username : req.body.username,
                    email : req.body.email,
                    password : hashedPassword,
                    favorites : []
                })
                await newUser.save();
                res.send("User created");
            }
        })
    },

    update : (req,res) => {
        User.findByIdAndUpdate({_id : req.params.id}, req.body)
        .then( (doc) => {
            res.send(doc)
        });
    }


}