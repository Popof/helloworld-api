const { restart } = require("nodemon");

const Meal = require('../models/meal');


// Méthodes des CRUD
module.exports = {


    getAll: (req, res) => {
        // TODO Utiliser la DB avec Mongoose!
        Meal.find()
        .then(meal => {
            res.status(200).json(meal);
        })
        .catch(error => {
            res.status(500).json(error);
        })
    },

    add: (req, res) => {
        // TODO Utiliser les models de Mongoose
        const meal = Meal(req.body);
        
        // TODO Ajouter les données dans la DB!
        meal.save()
            .then(meal=>{
                res.status(200).json(meal);
            })
            .catch(error=>{
                res.status(500).json(error);
            })

        res.status(200).json({message : 'Insert !'});
    },

    getById: (req, res) => {
        const {id} = req.params;

        Meal.findById(id)
            .then(meal => {
                res.status(200).json(meal);
            })
            .catch(error => {
                res.status(500).json(error);
            })

        res.status(200).json({message : ` Get ${id}`});
    },

    delete: (req, res) => {
        const {id} = req.params;

        Meal.findByIdAndDelete(id)
        .then(meal => {
            res.status(204);
        })
        .catch(error => {
            res.status(500).json(error);
        })

        res.status(200).json({message : `delete ${id}`});
    }

}