const mongoose = require('mongoose');


const userSchema = mongoose.Schema( 
    {
        username : 
        {
            type : String,
            required : true
        },
        email : 
        {
            type : String,
            required : true
        },
        password : 
        {
            type : String,
            required : true
        },
        favorites : 
        {
            type : [String],
            required : false
        }

        
    },
    {
        collection : 'Users',
        timestamps : true
    }
);

const User = mongoose.model('user', userSchema);
module.exports = User;

