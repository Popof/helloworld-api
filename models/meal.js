const mongoose = require('mongoose');

// Créer le schéma des données "meal"
// - Définir les champs (type, constraint, validation)
// - Ajouter des options au schéma 
const mealSchema = mongoose.Schema({
    name : {
        type : String,
        required : true
    },
    recipe : {
        type : [String],
        required : true
    },
    category : {
        type : [{catName : String, className : String }],
        required : true
    }
    ,
    count : {
        type : Number,
        default : 0

    },
    rating : {
        type : Number,
        default : 0
    },

    nb_people : {
        type : Number,
        required : true

    },

    ingredients : {
        type : [[Object]],
        required : true
    },

    image : {
        type: String
    }

},
// AJOUT D'OPTIONS
{
    collection : 'Meals',
    timestamps: true,
});

// Créer le modèle "meal" sur base du schéma
const Meal = mongoose.model('meal', mealSchema);
module.exports = Meal;