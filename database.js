const { urlConnection } = require ('./db.json')
const mongoose = require ('mongoose');

// Initialisation de la connexion vers MongoDB

mongoose.connect(urlConnection, 
    {
    useNewUrlParser: true,
    useUnifiedTopology: true
    })
    .then(() => {
        console.log ('MongoDB connexion OK');
    })
    .catch(() => {
        console.log('Connexion ERROR');

        //Kill app
        process.exit();
    });

const db = mongoose.connection;
module.exports = db;